import { Platform } from 'react-native';
import { BottomTabBar, createBottomTabNavigator, createDrawerNavigator, createStackNavigator } from 'react-navigation';
import Drawer from './components/drawer/Drawer';
import LoginScreen from './screens/authScreen/LoginScreen';
import NoticeDetailScreen from './screens/dashboardScreen/noticeScreen/NoticeDetailScreen';
import NoticeScreen from './screens/dashboardScreen/noticeScreen/NoticeScreen';
import ProfileScreen from './screens/dashboardScreen/profileScreen/ProfileScreen';
import ReportScreen from './screens/dashboardScreen/reportScreen/ReportScreen';
import WareReportDetailScreen from './screens/dashboardScreen/reportScreen/WareReportDetailScreen';
import WasteReportDetailScreen from './screens/dashboardScreen/reportScreen/WasteReportDetailScreen';
import ListPhoneGetDetailScreen from './screens/registerPasmandScreen/listPhoneGetDeliverWasteForContractor/ListPhoneGetDetailScreen';
import ListPhoneGetDetailNewScreen from './screens/registerPasmandScreen/listPhoneGetDeliverWasteForContractor/ListPhoneGetDetailNewScreen';
import CashWithdrawalScreen from './screens/creditManagementScreen/cashWithdrawalScreen/CashWithdrawalScreen';
import IntroScreen from './screens/introScreen/IntroScreen';
import SplashScreen from './screens/splashScreen/SplashScreen';
import Colors from './utility/Colors';
import AboutScreen from './screens/aboutScreen/AboutScreen';
import RegisterScreen from './screens/authScreen/RegisterScreen';
import QrcodeScreen from './screens/registerPasmandScreen/qrcodeScreen/QrcodeScreen';
import ListHelperScreen from './screens/registerPasmandScreen/listHelperScreen/ListHelperScreen';
import ListRequestPackageScreen from './screens/registerPasmandScreen/listRequestPackageScreen/ListRequestPackageScreen';
import ListRequestFixedBoothScreen from './screens/registerPasmandScreen/listRequestFixedBoothScreen/ListRequestFixedBoothScreen';
import ListPhoneGetDeliverWasteForContractor from './screens/registerPasmandScreen/listPhoneGetDeliverWasteForContractor/ListPhoneGetDeliverWasteForContractor';
import DeliveredwasteScreen from './screens/registerPasmandScreen/deliveredwasteScreen/DeliveredwasteScreen';
import ListHelperScreenDeliverWaste from './screens/registerPasmandScreen/listHelperScreenDeliverWaste/ListHelperScreenDeliverWaste';
import PhoneGetDeliverWasteForTransfer from './screens/phoneGetDeliverWasteForTransfer/PhoneGetDeliverWasteForTransfer';
import ListHelperTransferScreen from './screens/listHelperTransferScreen/ListHelperTransferScreen';
import DeliveredwasteNewScreen from './screens/registerPasmandScreen/deliveredwasteNewScreen/DeliveredwasteNewScreen';
import RequestDetailsScreen from './screens/registerPasmandScreen/requestDetailsScreen/RequestDetailsScreen';
import ListPaymentFixedBoothScreen from './screens/registerPasmandScreen/listPaymentFixedBoothScreen/ListPaymentFixedBoothScreen';
import DetailPaymentFixedBoothScreen from './screens/registerPasmandScreen/detailPaymentFixedBoothScreen/DetailPaymentFixedBoothScreen';
import DeliveryTypeScreen from './screens/creditManagementScreen/deliveryTypeScreen/DeliveryTypeScreen'
import ProductScreen from './screens/creditManagementScreen/productScreen/ProductScreen'


import { GetDim } from './utility/Constants';

const Dashboard = createBottomTabNavigator({
    notice: {
        screen: NoticeScreen
    },
    // wasteList: {
    //     screen: WasteListScreen
    // },
    // residue: {
    //     screen: ResidueScreen
    // },
    report: {
        screen: ReportScreen
    },
    profile: {
        screen: ProfileScreen,
    },
}, {
    initialRouteName: 'profile',
    swipeEnabled: false,
    tabBarOptions: {
        showLabel: false,
        activeTintColor: Colors.orange,
        inactiveTintColor: Colors.gray,
        style: {
            backgroundColor: '#e0e0e0',
            height: Platform.OS === 'android' ? 62 : 60,
        },
    },
    tabBarComponent: BottomTabBar,
    tabBarPosition: 'bottom'
});

const drawerWidth = (GetDim().width / 1.4 > 275) ? GetDim().width / 1.6 : GetDim().width / 1.2;

const DrawerScreen = createDrawerNavigator({
    drawer: { screen: Dashboard }
}, {
    contentComponent: Drawer,
    drawerWidth,
    drawerPosition: 'right',
    drawerType: 'slide',
    drawerLockMode: 'unlocked'
});

export const Router = createStackNavigator({
    splash: { screen: SplashScreen },
    intro: { screen: IntroScreen },
    login: { screen: LoginScreen },
    dashboard: { screen: DrawerScreen },
    noticeDetail: { screen: NoticeDetailScreen },
    wasteReportDetail: { screen: WasteReportDetailScreen },
    ListPhoneGetDetail:{screen:ListPhoneGetDetailScreen},
    ListPhoneGetDetailNew:{screen:ListPhoneGetDetailNewScreen},
    wareReportDetail: { screen: WareReportDetailScreen },
    CashWithdrawalScreen: { screen: CashWithdrawalScreen },
    AboutScreen: { screen: AboutScreen },
    RegisterScreen: { screen: RegisterScreen },
    QrcodeScreen: { screen: QrcodeScreen },
    ListHelperScreen: { screen: ListHelperScreen },
    ListRequestPackageScreen: { screen: ListRequestPackageScreen },
    ListRequestFixedBoothScreen: { screen: ListRequestFixedBoothScreen },
    ListPhoneGetDeliverWasteForContractor: { screen: ListPhoneGetDeliverWasteForContractor },
    DeliveredwasteScreen: { screen: DeliveredwasteScreen },
    ListHelperScreenDeliverWaste: { screen: ListHelperScreenDeliverWaste },
    PhoneGetDeliverWasteForTransfer: { screen: PhoneGetDeliverWasteForTransfer },
    ListHelperTransferScreen:{screen:ListHelperTransferScreen},
    DeliveredwasteNewScreen:{ screen: DeliveredwasteNewScreen },
    RequestDetailsScreen:{screen:RequestDetailsScreen},
    ListPaymentFixedBoothScreen:{screen:ListPaymentFixedBoothScreen},
    DetailPaymentFixedBoothScreen:{screen:DetailPaymentFixedBoothScreen},
    DeliveryTypeScreen:{screen:DeliveryTypeScreen},
    ProductScreen:{screen:ProductScreen},
}, {
    headerMode: 'none',
    initialRouteName: 'splash',
});
