import Constants from './../utility/Constants';
import WebService from './WebService';

export default class Repository
{

    // login
    static LoginApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneLoginContractor`, { method: 'POST', headers });
    }

    // user info
    static PhoneInfoContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneInfoContractor`, { method: 'POST', headers });
    }

    static PhoneChangePasswordContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneChangePasswordContractor`, { method: 'POST', headers });
    }

    static PhoneGetBankForContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetBankForContractor`, { method: 'POST', headers });
    }
    //PhoneAddRequestDeliverWasteForContractor
    static PhoneAddRequestDeliverWasteForContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneAddRequestDeliverWasteForContractor`, { method: 'POST', headers });
    }
    //PhoneGetDeliverWasteByDeliveryWasteIDForContractor
      static PhoneGetDeliverWasteByDeliveryWasteIDForContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliverWasteByDeliveryWasteIDForContractor`, { method: 'POST', headers });
    }
     //PhoneGetHelperDetails
     static PhoneGetHelperDetailsApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetHelperDetails`, { method: 'POST', headers });
    }
    //PhoneAddHelperByContractor
static PhoneAddHelperByContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddHelperByContractor`, { method: 'POST', headers });
}
//PhoneGetListRequestPackage
static PhoneGetListRequestPackageApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetListRequestPackage`, { method: 'POST', headers });
}
//PhoneGetWastTypeForContractor
static PhoneGetWastTypeForContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetWastTypeForContractor`, { method: 'POST', headers });
}
//PhoneAddDeliverWasteByContractor
static PhoneAddDeliverWasteByContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddDeliverWasteByContractor`, { method: 'POST', headers });
}
//PhoneAddRequestDeliverWasteByFixedBoothPerosnelApi
static PhoneAddRequestDeliverWasteByFixedBoothPerosnelApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddRequestDeliverWasteByFixedBoothPerosnel`, { method: 'POST', headers });
}
//PhoneSearchHelper
static PhoneSearchHelperApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneSearchHelper`, { method: 'POST', headers });
}
//PhoneGetDeliveredWasteContractorReportApi
static PhoneGetDeliveredWasteContractorReportApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliveredWasteContractorReport`, { method: 'POST', headers });
}
//PhoneAddLikeNotificationForContractor
static PhoneAddLikeNotificationForContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddLikeNotificationForContractor`, { method: 'POST', headers });
}
  //PhonePackageRegistration
  static PhonePackageRegistrationApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhonePackageRegistration`, { method: 'POST', headers });
}
 //PhoneDeliveryRequestPackage
 static PhoneDeliveryRequestPackageApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneDeliveryRequestPackage`, { method: 'POST', headers });
}


//PhoneRegisterDeliverWasteByContractor
static PhoneRegisterDeliverWasteByContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneRegisterDeliverWasteByContractor`, { method: 'POST', headers });
}
//PhoneGetDeliverWasteForContractor
static PhoneGetDeliverWasteForContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliverWasteForContractor`, { method: 'POST', headers });
}
      //PhoneGetMainAreasForContractor
static PhoneGetMainAreasForContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetMainAreasForContractor`, { method: 'POST', headers });
}
    //PhoneSettingsForContractor
static PhoneSettingsForContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneSettingsForContractor`, { method: 'POST', headers });
}
     //PhoneAddCashpaymentContractor
     static PhoneAddCashpaymentContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneAddCashpaymentContractor`, { method: 'POST', headers });
    }
    //PhoneAddCashpaymentToHelper
    static PhoneAddCashpaymentToHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneAddCashpaymentToHelper`, { method: 'POST', headers });
    }
    //notice
    static PhoneGetNotificationForContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetNotificationForContractor`, { method: 'POST', headers });
    }

    static PhoneGetNotificationByIDForContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetNotificationByIDForContractor`, { method: 'POST', headers });
    }


    //report
    static PhoneGetDeliveredWasteContractorReportApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliveredWasteContractorReport`, { method: 'POST', headers });
    }

    static PhoneGetDeliveredWasteReportItemContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliveredWasteReportItemContractor`, { method: 'POST', headers });
    }

    static PhoneGetFinancialContractorReportApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetFinancialContractorReport`, { method: 'POST', headers });
    }

    static PhoneGetReceivedWareContractorReportApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetReceivedWareContractorReport`, { method: 'POST', headers });
    }

    static PhoneGetReceivedWareContractorReportItemApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetReceivedWareContractorReportItem`, { method: 'POST', headers });
    }
    //PhoneAddLocationContractor
    static PhoneAddLocationContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneAddLocationContractor`, { method: 'POST', headers });
    }
  //PhoneGetDeliverWasteForTransfer
  static PhoneGetDeliverWasteForTransferApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliverWasteForTransfer`, { method: 'POST', headers });
}

    //PhoneGetPersonnelOfBooth
  static PhoneGetPersonnelOfBoothApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetPersonnelOfBooth`, { method: 'POST', headers });
} 
 //PhoneAddTransferWasteForContractor
 static PhoneAddTransferWasteForContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddTransferWasteForContractor`, { method: 'POST', headers });
} 
static PhoneGetInfoCooperation2Api(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetInfoCooperation2`, { method: 'POST', headers });
} 
//PhoneGetWareForContractor
static PhoneGetWareForContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetWareForContractor`, { method: 'POST', headers });
} 
//PhoneAddRequestWareForHelperByContractor
static PhoneAddRequestWareForHelperByContractorApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddRequestWareForHelperByContractor`, { method: 'POST', headers });
} 
}
