import { StyleProvider } from 'native-base';
import { Alert } from 'react-native';
import React, { Component } from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { createAppContainer } from 'react-navigation';
import { connect, Provider } from 'react-redux';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import store from './redux/store';
import { Router } from './Router';
import LocationSender from './backgroundTask/locationSender';

class App extends Component {

    render() {
        const AppContainer = createAppContainer(Router);
        const RouterWithRedux = connect()(AppContainer);
        return (
            <StyleProvider style={getTheme(material)}>
                <Provider store={store}>
                    <LocationSender />
                    <PaperProvider>
                        <RouterWithRedux/>
                    </PaperProvider>
                </Provider>
            </StyleProvider>
        );
    }
}

export default App;
