import { Icon, Picker } from 'native-base';
import React, { Component } from 'react';
import { ScrollView, StatusBar, Text, TextInput, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Portal, Dialog } from 'react-native-paper';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { Button, Loading } from '../../../components';
import HeaderMain2 from '../../../components/HeaderMain2';
import TabItem from '../../../components/TabItem';
import { setUser, changePassword, activeLocation } from '../../../redux/actions';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';

class ProfileScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'پروفایل'}
                    source={require('./../../../assets/image/ic_home.png')}
                    color={tintColor}
                />
        };
    };
    
    constructor(props) {
        super(props);
        const {
            AccountNumber,
            AccountOwner,
            BankID,
            BankTitle,
            ContractorId,
            IdentificationCode,
            Mobile,
            NameFamily,
            ShabaNumber,
        } = this.props.userData.user;
        const {
            UserName,
            Password
        } = this.props.userData;
        this.state = {
            isLoadingPage: true,
            isLoadingButton: false,
            isActive: false,
            isChangeActive: false,

            bankList: [],
            areaList: [],

            NameFamily,
            UserName,
            Password,
            ContractorId,
            IdentificationCode,
            Mobile,
            AccountNumber,
            AccountOwner,
            BankID,
            BankTitle,
            ShabaNumber,

            NewPassword: '',
            RepeatNewPassword: '',
            OldPassword: '',

            errorNameFamily: '',
            errorMobileNumber: '',
            errorBank: '',
            errorNewPassword: '',
            errorOldPassword: '',
            errorRepeatNewPassword: '',

            apiSuccess: false,
            apiError: false,
            apiErrorDesc: '',
            location:null
        };

        this.onConfirmPress = this.onConfirmPress.bind(this);
    }

    onActiveLocationButtonPressed() {
        this.props.activeLocation(!this.props.isActive);
    }
 
    componentDidMount = () => {
        this.getApi();
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {

    }

    onConfirmPress = () => {
        const {
            NameFamily,
            Mobile,
            Password,
            OldPassword,
            NewPassword,
            RepeatNewPassword,
            ShabaNumber,
            AccountNumber,
            AccountOwner,
            BankID
        } = this.state;

        let error = false;

        if (NameFamily.length === 0) {
            error = true;
            this.setState({
                errorNameFamily: 'نام و نام خانوادگی نمیتواند خالی باشد'
            });
        }

        if (Mobile.length === 0) {
            error = true;
            this.setState({
                errorMobileNumber: 'شماره همراه نمیتواند خالی باشد'
            });
        }

        if (OldPassword.length > 0) {
            if (NewPassword.length === 0) {
                error = true;
                this.setState({
                    errorNewPassword: 'رمز عبور جدید وارد نشده است'
                });
            }
            if (RepeatNewPassword.length === 0) {
                error = true;
                this.setState({
                    errorRepeatNewPassword: 'تکرار رمز عبور وارد نشده است'
                });
            }
            if (NewPassword !== RepeatNewPassword) {
                error = true;
                this.setState({
                    errorRepeatNewPassword: 'تکرار رمز عبور درست نیست'
                });
            }
        }

        if (ShabaNumber.length === 0 && AccountOwner.length === 0) {
            error = true;
            this.setState({
                errorBank: 'اطلاعات حساب نمیتواند خالی باشد'
            });
        }

        if (!error) {
            this.setState({
                isLoadingButton: true
            }, () => {
                this.phoneChangePasswordContractorApi();
            });
        }
    }

    getApi = async () => {
        await this.phoneInfoContractorApi();
        await this.getBankApi();
    }

    async getBankApi() {
        const headers = null;
        const params = {
            UserName: this.props.userData.UserName,
            Password: this.props.userData.Password,
        };
        try {
            const response = await Repository.PhoneGetBankForContractorApi(params, headers);

            if (response.Success === 1) {
                const list = response.Result;
                this.setState({
                    bankList: list,
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingPage: false,
            });
        } catch (error) {
            this.setState({
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    async phoneInfoContractorApi() {
        const headers = null;
        const params = {
            UserName: this.props.userData.UserName,
            Password: this.props.userData.Password,
        };
        try {
            const response = await Repository.PhoneInfoContractorApi(params, headers);
            if (response.Success === 1) {
                this.props.setUser(response.Result);
                const {
                    AccountNumber,
                    AccountOwner,
                    BankID,
                    BankTitle,
                    ContractorId,
                    IdentificationCode,
                    Mobile,
                    NameFamily,
                    ShabaNumber,
                } = response.Result;
                this.setState({
                    AccountNumber,
                    AccountOwner,
                    BankID,
                    BankTitle,
                    ContractorId,
                    IdentificationCode,
                    Mobile,
                    NameFamily,
                    ShabaNumber,
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    async phoneChangePasswordContractorApi() {
        const headers = null;
        const params = {
            UserName: this.props.userData.UserName,
            Password: this.state.NewPassword.length > 0 ? this.state.NewPassword : this.props.userData.Password,
            OldPassword: this.state.OldPassword.length > 0 ? this.state.OldPassword : this.props.userData.Password,
            ShabaNumber: this.state.ShabaNumber,
            AccountNumber: this.state.AccountNumber,
            AccountOwner: this.state.AccountOwner,
            BankID: this.state.BankID,
            NameFamily: this.state.NameFamily,
            Mobile: this.state.Mobile
        };
        try {
            const response = await Repository.PhoneChangePasswordContractorApi(params, headers);
            if (response.Success === 1) {
                
                if (this.state.NewPassword.length > 0) {
                    this.props.changePassword({ Password: this.state.NewPassword });
                }
                this.setState({
                    apiSuccess: true,
                    apiSuccessDesc:response.Text,
                    apiSuccessTitle:response.Titel,
                    isLoadingPage: true,
                    NewPassword: '',
                    OldPassword: '',
                    RepeatNewPassword: ''
                }, () => {
                    this.phoneInfoContractorApi();
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingButton: false,
            });
        } catch (error) {
            await this.setState({
                isLoadingButton: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    renderBankPickerItem() {
        return this.state.bankList.map(bank =>
            <Picker.Item key={bank.BankID.toString()} label={`${bank.Title}`} value={bank.BankID} />
        );
    }

    render() {
        return this.state.isLoadingPage ? (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                 <HeaderMain2
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                    headerTitle={'پروفایل '}
                />
                <Loading
                    message={'در حال دریافت اطلاعات'}
                    messageColor={Colors.green}
                />
            </View>
        ) :
            (
                <View style={styles.container}>
                    <StatusBar
                        backgroundColor={Colors.statusBar}
                        barStyle="light-content"
                    />
                    <HeaderMain2
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                    
                    headerTitle={'پروفایل '}
                />
                    <ScrollView>
                        <View style={styles.dashboardItemContainer}>
                            <View style={styles.dashboardItemUserContainer}>
                                <View style={styles.userImageContainer} >
                                    <FastImage
                                        source={require('./../../../assets/image/ic_profile.png')}
                                        resizeMode={FastImage.resizeMode.contain}
                                        style={styles.userImage}
                                    />
                                </View>
                                <Text style={styles.dashboardItemTextBoldSamll}>
                                    {this.props.userData.user.NameFamily}
                                </Text>
                                <Text style={styles.dashboardItemTextBoldSamll}>
                                    {this.props.userData.user.Mobile}
                                </Text>
                                <Button
                                    buttonText= {this.props.isActive ? 'پایان فعالیت ' : 'شروع فعالیت '}
                                    style={styles.button}
                                    colors= {!this.props.isActive ? ['#4caf50', '#4caf50'] : [Colors.red, Colors.red]}
                                    onPress={() => {
                                        this.onActiveLocationButtonPressed();
                                    }}
                                />
                            </View>
                        </View>
                        <View style={styles.separator} />
                        <Text style={styles.inputTitle}>
                            {'نام و نام خانوادگی'}
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'نام و نام خانوادگی خود را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                editable={false}
                                style={styles.input}
                                value={this.state.NameFamily}
                                onChangeText={(NameFamily) => {
                                    this.setState({
                                        NameFamily,
                                        errorNameFamily: ''
                                    });
                                }}
                                
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_person.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {this.state.errorNameFamily.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorNameFamily}
                            </Text>
                        }
                        <Text style={styles.inputTitle}>
                            {'شماره همراه'}
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'شماره همراه خود را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                editable = {false}
                                style={styles.input}
                                value={this.state.Mobile}
                                onChangeText={(Mobile) => {
                                    this.setState({
                                        Mobile,
                                        errorMobileNumber: ''
                                    });
                                }}
                                
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_mobile.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {this.state.errorMobileNumber.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorNameFamily}
                            </Text>
                        }
                        <Text style={styles.inputTitle}>
                            {'رمز عبور'}
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'رمز عبور خود را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                style={styles.input}
                                value={this.state.OldPassword}
                                onChangeText={(OldPassword) => {
                                    this.setState({
                                        OldPassword,
                                        errorOldPassword: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_person.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {this.state.errorOldPassword.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorOldPassword}
                            </Text>
                        }
                        <Text style={styles.inputTitle}>
                            {'رمز عبور جدید'}
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'رمز عبور جدید خود را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                style={styles.input}
                                value={this.state.NewPassword}
                                onChangeText={(NewPassword) => {
                                    this.setState({
                                        NewPassword,
                                        errorNewPassword: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_person.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {this.state.errorNewPassword.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorNewPassword}
                            </Text>
                        }
                        <Text style={styles.inputTitle}>
                            {'تکرار رمز عبور جدید'}
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'تکرار رمز عبور جدید خود را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                style={styles.input}
                                value={this.state.RepeatNewPassword}
                                onChangeText={(RepeatNewPassword) => {
                                    this.setState({
                                        RepeatNewPassword,
                                        errorNameFamily: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_person.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {this.state.errorRepeatNewPassword.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorRepeatNewPassword}
                            </Text>
                        }
                        <Text style={styles.inputTitle}>
                            {'اطلاعات حساب بانکی'}
                        </Text>
                        <TextInput
                            placeholder={'شمار شبا'}
                            placeholderTextColor={Colors.textGray}
                            selectionColor={Colors.textBlack}
                            keyboardType={'default'}
                            autoFocus={false}
                            style={styles.inputSingle}
                            value={this.state.ShabaNumber}
                            onChangeText={(ShabaNumber) => {
                                this.setState({
                                    ShabaNumber,
                                    errorBank: ''
                                });
                            }}
                            editable={this.state.isChangeActive}
                        />
                        <TextInput
                            placeholder={'نام صاحب حساب'}
                            placeholderTextColor={Colors.textGray}
                            selectionColor={Colors.textBlack}
                            keyboardType={'default'}
                            autoFocus={false}
                            style={styles.inputSingle}
                            value={this.state.AccountOwner}
                            onChangeText={(AccountOwner) => {
                                this.setState({
                                    AccountOwner,
                                    errorBank: ''
                                });
                            }}
                            editable={this.state.isChangeActive}
                        />
                        <View style={styles.pickerContainer}>
                            <Picker
                                style={styles.pickerStyle}
                                textStyle={styles.pickerTextStyle}
                                iosHeader="بانک را انتخاب کنید"
                                headerBackButtonText="بستن"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                headerStyle={styles.pickerHeaderStyle}
                                headerTitleStyle={styles.pickerHeaderTextStyle}
                                headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
                                itemStyle={styles.pickerItemStyle}
                                itemTextStyle={styles.pickerItemTextStyle}
                                selectedValue={this.state.BankID}
                                onValueChange={(BankID) => {
                                    this.setState({ BankID });
                                }}
                                mode={'dropdown'}
                                enabled={this.state.isChangeActive}
                            >
                                {this.renderBankPickerItem()}
                            </Picker>
                        </View>
                        {this.state.errorBank.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorBank}
                            </Text>
                        }
                        {this.state.isChangeActive &&
                            <Button
                                buttonText={this.state.isLoadingButton ? 'لطفا منتظر بمانید' : 'اعمال تغییرات'}
                                style={styles.button}
                                onPress={this.onConfirmPress}
                                isLoading={this.state.isLoadingButton}
                            />
                            ||
                            <Button
                                buttonText={'تغییر اطلاعات'}
                                style={styles.button}
                                onPress={() => {
                                    this.setState(prevState => {
                                        return {
                                            isChangeActive: !prevState.isChangeActive
                                        };
                                    });
                                }}
                            />
                        }
                    </ScrollView>
                    <Portal>
                        <Dialog
                            visible={this.state.apiError}
                            style={styles.dialogContainer}
                            dismissable={false}
                        >
                            <Dialog.Content>
                                <Text style={styles.dialogText}>
                                    {this.state.apiErrorDesc}
                                </Text>
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button
                                    buttonText={'تلاش مجدد'}
                                    onPress={() => {
                                        this.setState({
                                            apiError: false,
                                            isLoadingPage: true,
                                        }, () => {
                                            this.getApi();
                                        });
                                    }}
                                    style={styles.dialogButton}
                                />
                            </Dialog.Actions>
                        </Dialog>
                        <Dialog
                            visible={this.state.apiSuccess}
                            style={styles.dialogContainer}
                            onDismiss={() => {
                                this.setState({
                                    apiSuccess: false,
                                });
                            }}
                        >
                         <Dialog.Title style={styles.dialogTitleText}>
                            {this.state.apiSuccessTitle}
                         </Dialog.Title>
                            <Dialog.Content style={styles.dialogContent}>

                               
                                <Text style={styles.dialogText}>
                                    {this.state.apiSuccessDesc}
                                </Text>
                                <Button
                                    buttonText={'بستن'}
                                    onPress={() => {
                                        this.setState({
                                            apiSuccess: false,
                                        });
                                    }}
                                    style={styles.dialogButton}
                                />
                            </Dialog.Content>
                           
                        </Dialog>
                    </Portal>
                </View>
            );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },

    //dashboard style
    dashboardItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: hp('30%'),
    },
    dashboardItemUserContainer: {
        justifyContent: 'center'
    },
    userImageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: wp('15%'),
        width: wp('15%'),
        borderRadius: wp('10%'),
        borderColor: Colors.textGray,
        borderWidth: 1,
        overflow: 'hidden',
    },
    userImage: {
        height: wp('15%'),
        width: wp('15%'),
    },
    dashboardItemRankContainer: {
        justifyContent: 'center'
    },
    dashboardItemHelpContainer: {
        justifyContent: 'center'
    },
    dashboardItemTextBoldSamll: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dashboardItemTextBoldLarge: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dashboardItemTextGray: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textGray,
    },
    dashboardItemDetailButton: {
        padding: 5,
        borderRadius: 5,
        borderColor: Colors.textGray,
        borderWidth: 1
    },
    separator: {
        height: 0.6,
        backgroundColor: Colors.border,
    },

    //edit style
    inputTitle: {
        width: wp('70%'),
        alignSelf: 'center',
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.textGray,
        textAlign: 'right',
        marginBottom: 10
    },
    inputContainer: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderRadius: 5,
        borderWidth: 1,
        marginBottom: 10,
        paddingRight: 10,
        paddingLeft: 10,
        overflow: 'hidden'
    },
    inputIcon: {
        height: wp('7%'),
        width: wp('7%'),
        marginLeft: 10
    },
    input: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'right',
    },
    inputSingle: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 10
    },
    inputBirthDay: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        height: hp('6%'),
        width: wp('21%'),
        marginLeft: 5,
        marginRight: 5,
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 10
    },
    inputAddressContainer: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: hp('15%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderRadius: 5,
        borderWidth: 1,
        marginBottom: 10,
        paddingRight: 10,
        paddingLeft: 10,
        overflow: 'hidden'
    },
    inputAddress: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        alignSelf: 'center',
        textAlign: 'center',
        textAlignVertical: 'top'
    },
    button: {
        borderColor: Colors.border,
        borderRadius: 5,
        borderWidth: 1,
        width: wp('70%')
    },
    buttonRed: {
        borderColor: Colors.border,        
        borderRadius: 5,
        borderWidth: 1,
        width: wp('70%')
    },
    error: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.error,
        textAlign: 'center'
    },

    sexActive: {
        backgroundColor: Colors.green,
        height: hp('6%'),
        width: wp('20%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },
    sexDeactive: {
        height: hp('6%'),
        width: wp('20%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },
    sexActiveText: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#ffffff',
    },
    sexDeactiveText: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
    },

    pickerContainer: {
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
    },
    pickerStyle: {
        // borderBottomWidth: 0.8,
        // borderColor: '#e2e2e2',
        width: wp('50%'),
        // justifyContent: 'center',
    },
    pickerHeaderStyle: {
        backgroundColor: Colors.purple,
    },
    pickerHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'right',
    },
    pickerBackHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'left',
    },
    pickerTextStyle: {
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: '#34495e',
        textAlign: 'center',
    },
    pickerItemStyle: {
        width: wp('90%'),
        backgroundColor: 'transparent'
    },
    pickerItemTextStyle: {
        fontSize: 16,
        width: wp('65%'),
        backgroundColor: 'transparent',
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: '#34495e',
        textAlign: 'right'
    },
    pickerSeparator: {
        '@media android': {
            height: 1,
            width: wp('90%'),
            backgroundColor: '#e2e2e2'
        }
    },

    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData,
        isActive: state.userData.isActive
    };
};

const mapActionToProps = {
    setUser,
    changePassword,
    activeLocation
    
};

export default connect(mapStateToProps, mapActionToProps)(ProfileScreen);
