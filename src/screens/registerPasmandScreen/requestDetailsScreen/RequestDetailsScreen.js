import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,ScrollView } from 'react-native';
import { Text,Card, CardItem,Container, Content, Icon,Footer, FooterTab} from 'native-base';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { normalize } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';
import Repository from '../../../repository/Repository';

class RequestDetailsScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'نوع پسماند'}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };
    constructor(props) {
      super(props);
      this.state = {
          isLoadingPage: true,
          list: [],
          CreateDateStr:'',
          CreateTimeStr:'',
          DeliveryDateStr:'',
          DeliveryTimeStr:'',
          DeliveryWasteID:0,
          StatusStr:'',
          refreshing: false,
          loadingData: false,
          laps:[],
          apiError: false,
          apiErrorDesc: ''
      };
    }
    componentDidMount = async () => {
      var DeliveryWasteID = this.props.navigation.getParam('DeliveryWasteID',0);
     await this.setState({DeliveryWasteID:DeliveryWasteID});
      this.getApi();
  }

  
getApi = async () => {
  await this.PhoneGetDeliverWasteByDeliveryWasteIDForContractorApi();
}
async PhoneGetDeliverWasteByDeliveryWasteIDForContractorApi() {
  const headers = null;
  
  const params = {
    UserName: this.props.userData.UserName,
    Password: this.props.userData.Password,
    DeliveryWasteID:this.state.DeliveryWasteID
  };
//  alert(JSON.stringify(params));
  try {
      const response = await Repository.PhoneGetDeliverWasteByDeliveryWasteIDForContractorApi(params, headers);
      if (response.Success === 1) {
        alert("ثبت با موفقیت انجام شد");
          const laps = response.Result.Result;
          this.setState({
            laps:laps,
            StatusStr:response.Result.StatusStr,
            CreateDateStr:response.Result.CreateDateStr,
            CreateTimeStr:response.Result.CreateTimeStr,
            DeliveryDateStr:response.Result.DeliveryDateStr,
            DeliveryTimeStr:response.Result.DeliveryTimeStr,
            DeliveryWasteID:response.Result.DeliveryWasteID,
          });
      }
      else {
          this.setState({
              apiError: true,
              apiErrorDesc: response.Text
          });
      }
      this.setState({
          loadingData: false,
          refreshing: false,
          isLoadingPage: false,
      });
  } catch (error) {
      await this.setState({
          loadingData: false,
          refreshing: false,
          isLoadingPage: false,
          apiError: true,
          apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
  }
}

lapsList() {

  return this.state.laps.map((data) => {
    return (
      <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
              <Card style={[styles.CardSelect]}>
              <TouchableOpacity
                style={[styles.Btn]}
                onPress={() => this.props.navigation}
              >
            </TouchableOpacity>
              <TouchableOpacity
                style={[styles.Btn]}
                onPress={() => this.props.navigation}
              >
            </TouchableOpacity>
            <Text style={[styles.text]}>{data.HelperPrice} تومان</Text>
              <Text style={[styles.text]}>{data.Points} امتیاز - </Text>
               <TouchableOpacity
                style={[styles.Btn]}
                 onPress={() => this.props.navigation}
                  >
            </TouchableOpacity>
            <Text style={[styles.text]}> {data.WasteTypeCount} کیلوگرم </Text>
            <TouchableOpacity
                style={[styles.Btn]}
                onPress={() => this.props.navigation}
              >
            </TouchableOpacity>
            <Text style={[styles.text]}>{data.WasteTypeStr}</Text>
              </Card>
              </View>
    )
  })
  
  }
    render() {
        return (
            <Container>
              <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                 <HeaderBack
                        onBackPress={() => this.props.navigation.goBack()}
                        headerTitle={'مشاهده جزئیات سفارش '}
                    />
          <Content>
          <View style={[styles.Title]}>
            <View  style={{flex:9}}>
                <Text style={[styles.textTitle]}> نوع پسماند </Text>
            </View>
            <View style={{flex:1,flexDirection:'row',justifyContent:"flex-end",alignItems:'center',}}>
               <Icon type="Entypo" name="shopping-bag" style={{color:'#50b3ae'}}/>
            </View>
          </View>
          <View style={[styles.TotalCard]}>
          {this.lapsList()}
            
              
          {/* <View style={[styles.Title]}>
            <View  style={{flex:9}}>
                <Text style={[styles.textTitle]}> زمان تحویل</Text>
            </View>
            <View style={{flex:1,flexDirection:'row',justifyContent:"flex-end",alignItems:'center',}}>
               <Icon type="Entypo" name="shopping-bag" style={{color:'#50b3ae'}}/>
            </View>
          </View> */}
          {/* <View style={[styles.TotalCard]}>
            <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
              <Card style={[styles.CardTime]}>
             
              <TouchableOpacity
                style={[styles.BtntTime]}
                onPress={() => this.props.navigation}
              >
                  <Icon type="Entypo" name="edit" style={[styles.IconGreen]}/>
            </TouchableOpacity>
           <View style={{flexDirection:'column',alignItems: 'center',justifyContent:'center'}}>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.CreateDateStr}</Text>
                <Text style={[styles.text]}>{this.state.DeliveryDateStr}</Text>
               </View>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:5}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.DeliveryTimeStr}</Text>
                <Text style={[styles.text]}>ساعت</Text>
               </View>
           </View>
              </Card>
              </View>
              </View> */}
             </View>
             
        </Content>
        <Footer style={{height:70,width:"100%"}}>
          <FooterTab  style={{backgroundColor:'#50b3ae'}}>
          
           <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <TouchableOpacity 
                style={[styles.BtnFooter]}
                onPress={() => this.props.navigation.navigate("dashboard")}
                                     >
                <Text style={[styles.textFooter,{color:'white',}]}>
                      بازگشت به صفحه اصلی
                </Text>
            </TouchableOpacity>
            </View>
          </FooterTab>
          </Footer>
      </Container>
        );
    }

}

  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     header:{
      backgroundColor:'#50b3ae',
     },
      textHeader:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
      text:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
       },
       textTitle:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
       },
       Title:{
        flex:1,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'flex-end',
        paddingTop:15,
        paddingHorizontal:8
       },
       TotalCard:{
        flex:1,
        flexDirection:'column',
        alignItems: 'center',
        justifyContent:'center',
        paddingHorizontal:10,
        paddingTop:10
      },
      CardSelect:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-around',
        borderRadius:5,
        padding:8,
        paddingVertical:12
      },
      IconGreen:{
        color:'#50b3ae', 
        fontSize: 20
      },
      IconBag:{
        color:'#50b3ae',
        fontSize:25
      },
      textFooter:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
       BtntTime:{
           justifyContent:'center',
           alignItems:'center',
           
       },
       CardTime:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-between',
        borderRadius:5,
        paddingHorizontal:12,
        paddingVertical:10
      },
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(RequestDetailsScreen);
