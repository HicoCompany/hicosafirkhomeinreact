export const SET_USER = 'SET_USER';

export const SET_IS_SEEN_INTRODUCE = 'SET_IS_SEEN_INTRODUCE';
export const SET_IS_LOGIN = 'SET_IS_LOGIN';
export const SET_DATA_LOGIN = 'SET_DATA_LOGIN';
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';

export const EXIT_APP = 'EXIT_APP';
export const ACTIVE_LOCATION = 'ACTIVE_LOCATION';


