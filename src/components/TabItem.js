import React from 'react';
import { Image, Text, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Colors from '../utility/Colors';
import { moderateScale, normalize } from './../responsive';

const TabItem = ({ source, name, color }) =>
    <View style={tabIconStyle.container}>
        <Image
            source={source}
            style={[tabIconStyle.icon, { tintColor: color }]}
            resizeMode={'contain'}
        />
        <Text style={[tabIconStyle.name, { color }]}>
            {name}
        </Text>
    </View>;

const tabIconStyle = EStyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%'
    },
    iconContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        height: moderateScale(26),
        width: moderateScale(26),
    },
    name: {
        '@media ios': {
            // fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        fontSize: normalize(11),
        color: Colors.gray,
        textAlign: 'center'
    }
});

export default TabItem;
