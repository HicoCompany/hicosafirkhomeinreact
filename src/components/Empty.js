import React from 'react';
import { Text, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { normalize } from '../responsive';
import Colors from '../utility/Colors';

const Empty = ({ text }) => {
    return (
        <View style={noDataStyle.container} >
            <Text style={noDataStyle.text}>
                {((text !== null) && (text !== undefined)) &&
                    text
                    ||
                    'موردی برای نمایش وجود ندارد'
                }
            </Text>
        </View>
    );
};

const noDataStyle = EStyleSheet.create({
    container: {
        backgroundColor: Colors.backgroundColor,
        height: '100%',
        width: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    text: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: Colors.green,
        fontSize: normalize(13),
        textAlign: 'center',
    }
});

export { Empty };

