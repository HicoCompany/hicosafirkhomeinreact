import { persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import UserReducer from './UserReducer';

const config = {
    key: 'primary',
    storage,
    blacklist: []
};

const rehydrated = (state = false, action) => {
    switch (action.type) {
        case 'persist/REHYDRATE' :
            return true;
        default:
            return state;
    }
};

export default persistCombineReducers(config, {
    rehydrated,
    userData: UserReducer,
});
