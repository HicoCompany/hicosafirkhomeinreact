import update from 'immutability-helper';
import {
    SET_USER,
    SET_IS_LOGIN,
    SET_IS_SEEN_INTRODUCE,
    SET_DATA_LOGIN,
    EXIT_APP,
    CHANGE_PASSWORD,
    ACTIVE_LOCATION
} from '../actions/types';

const INITIAL_STATE = {
    isSeenIntroduce: false,
    isLogin: false,
    isActive: false,
    UserName: '',
    Password: '',

    user: {
        AccountNumber: '',
        AccountOwner: '',
        BankID: -1,
        BankTitle: '',
        ContractorId: -1,
        IdentificationCode: '',
        Mobile: '',
        NameFamily: '',
        ShabaNumber: '',
    }
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_USER:
            return {
                ...state,
                user: action.payload
            };
        case SET_IS_LOGIN:
            return {
                ...state,
                isLogin: true
            };
        case SET_IS_SEEN_INTRODUCE:
            return {
                ...state,
                isSeenIntroduce: true,
            };
        case SET_DATA_LOGIN:
            return {
                ...state,
                UserName: action.payload.UserName,
                Password: action.payload.Password
            };

        case CHANGE_PASSWORD:
            return {
                ...state,
                Password: action.payload.Password
            };

        case ACTIVE_LOCATION:
            return {
                ...state,
                isActive: action.payload
            };

        case EXIT_APP:
            return {
                ...state,
                isSeenIntroduce: false,
                isLogin: false,
                UserName: '',
                Password: '',
            
                user: {
                    AccountNumber: '',
                    AccountOwner: '',
                    BankID: -1,
                    BankTitle: '',
                    ContractorId: -1,
                    IdentificationCode: '',
                    Mobile: '',
                    NameFamily: '',
                    ShabaNumber: '',
                }
            };

        default:
            return state;
    }
};
