import React, {Component} from 'react';
import { connect } from 'react-redux';
import { StatusBar } from 'react-native';
import Colors from '../../utility/Colors';
import HeaderBack from '../../components/HeaderBack';
import Repository from '../../repository/Repository';

import { View,Image,TouchableOpacity,ScrollView,Linking} from 'react-native';
import { Container, Header,Content, Footer, FooterTab, Button, 
   Icon, Text,Card, CardItem ,Badge} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';

class AboutScreen extends Component {
  handleClick = () => {
    
     
        Linking.openURL(this.state.list.Web);
      
   
  };
  handleClickDial = () => {
    
     
  Linking.openURL(`tel:`+this.state.list.Tel)
  

};

  constructor(props) {
    super(props);

    this.state = {
      horizontal: false,
      list: [],
    };
}
componentDidMount = () => {
  this.getApi();
}
getApi = async () => {
  await this.PhoneSettingsForContractorApi();
  
}
async PhoneSettingsForContractorApi() {
  const headers = null;
  const params = {
    UserName: this.props.userData.UserName,
    Password: this.props.userData.Password,
  };
  try {
      const response = await Repository.PhoneSettingsForContractorApi(params, headers);
      if (response.Success === 1) {

          const list = response.Result;
          this.setState({
            list: list,
           
          });
      }
      else {
          this.setState({
              apiError: true,
              apiErrorDesc: response.Text
          });
      }
      this.setState({
          isLoadingPage: false,
      });
  } catch (error) {
      this.setState({
          isLoadingPage: false,
          apiError: true,
          apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
  }
}
  static navigationOptions = ({ navigation }) => {
    
    return {
     
        tabBarIcon: ({ tintColor,horizontal }) =>
            <TabItem
                name={'درباره ما'}
                source={require('./../../assets/image/ic_notice.png')}
                color={tintColor}
            />,
    };
};

  render() {
    return (
        <Container>
           <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'درباره ما'}
                />
          <Content>
              <View style={[styles.center,{paddingTop:20}]}>
                 <Image source={require('./../../assets/image/About.png')} style={styles.ImgProduct}/>  
              </View>
         
              <View style={[styles.center,{paddingTop:20}]}>
                 <TouchableOpacity
                   style={[styles.BtnContent]}
                   onPress={this.handleClick}
                     >
                     <Text style={[styles.textBtn]}>{this.state.list.Web}</Text>
                  </TouchableOpacity>
                 </View>
                 <View style={[styles.center,{paddingTop:20}]}>
                 <TouchableOpacity
                   style={[styles.BtnContent]}
                   onPress={this.handleClickDial}
                     >
                     <Text style={[styles.textBtn]}>{this.state.list.Tel}</Text>
                  </TouchableOpacity>
                 </View>
                 {/* <View style={{flex:1,flexDirection:'row', alignItems:'center',justifyContent:'center',paddingTop:50}}>
                  <TouchableOpacity
                    style={[styles.Btn]}
                      onPress={() => this.props.navigation}
                      >
                    <View style={[styles.IconTotal]}>
                      <Icon type="FontAwesome" name="telegram" style={{color:'#1e98cb',fontSize:30}}/>
                    </View>
                 </TouchableOpacity>
                 <TouchableOpacity
                    style={[styles.Btn,{marginLeft:20,marginRight:20}]}
                      onPress={() => this.props.navigation}
                      >
                    <View style={[styles.IconTotal]}>
                      <Icon type="AntDesign" name="instagram" style={{color:'#e85556',fontSize:30}}/>
                    </View>
                 </TouchableOpacity>
                 <TouchableOpacity
                    style={[styles.Btn]}
                      onPress={() => this.props.navigation}
                      >
                    <View style={[styles.IconTotal]}>
                    <Icon type="Entypo" name="mail" style={{color:'#d71d60',fontSize:30}}/>
                    </View>
                 </TouchableOpacity>
                 </View> */}
                 <View style={[styles.center,{paddingTop:20}]}>
                   <Card style={styles.Box}>
                    <CardItem>
                      <Text style={styles.text}> با معرفی به دوستان و آشنایان خود , به حفظ محیط زیست کمک کنید</Text>
                   </CardItem>
                  </Card>
              </View>
              {/* <View style={[styles.center,{paddingTop:20,flex:1,flexDirection:'row'}]}>
              <TouchableOpacity
                    style={[styles.Btn]}
                      onPress={() => this.props.navigation}
                      >
                    <Text style={[styles.textGreen]}>سوالات متداول  </Text>
                   
                 </TouchableOpacity>
                 <Icon type="Entypo" name="dot-single" style={{color:'#78817e',fontSize:30}}/>
              <TouchableOpacity
                    style={[styles.Btn]}
                      onPress={() => this.props.navigation}
                      >
                    <Text style={[styles.textGreen]}>حریم خصوصی </Text>
                   
                 </TouchableOpacity>
              <Icon type="Entypo" name="dot-single" style={{color:'#78817e',fontSize:30}}/>
              <TouchableOpacity
                    style={[styles.Btn]}
                      onPress={() => this.props.navigation}
                      >
                    <Text style={[styles.textGreen]}>شرایط استفاده</Text>
                   
                 </TouchableOpacity>
                
              </View> */}
        </Content>

      </Container>
    );
  }

}


const styles = EStyleSheet.create({
  IRANSansNormal: {
    '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
   },
   IRANSansBold: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_B',
  },
   },
   header:{
    backgroundColor:'#50b3ae',
   },
    textHeader:{
      color:'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
    textBtn:{
      color:'#e2f3ba',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     text:{
      color:'black',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     ImgProduct:{
      width:'85%',
      height: 80,
      resizeMode:'contain',
     
    },
     center:{
         alignItems:'center',
         justifyContent:'center'
     },
     BtnContent:{
       backgroundColor:'#50b3ae',
         width:'90%',
         padding:10,
         borderRadius: 5,
         alignItems:'center',
         justifyContent:'center',
      
     },
     IconTotal:{
      flexDirection:'row',
      justifyContent:"center",
      alignItems:'center'
    },
    Box:{
      width:'90%',
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent:'center',
      borderRadius: 5,
      borderColor:'#8a987f'
    },
    textGreen:{
      color: '#50b3ae',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
    },
});
 
const mapStateToProps = (state) => {
  return {
      userData: state.userData
  };
};

const mapActionToProps = {

};
export default connect(mapStateToProps, mapActionToProps)(AboutScreen);
