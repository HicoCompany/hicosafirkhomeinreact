import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icons from 'react-native-vector-icons/MaterialIcons';
import { hp, normalize } from '../responsive';
import Colors from '../utility/Colors';

const HeaderModal = ({ onClosePress, headerText }) => {
    return (
        <View style={modalHeaderStyle.header}>
            <View style={modalHeaderStyle.leftButton}>
                <TouchableOpacity
                    style={modalHeaderStyle.closeContainer}
                    onPress={onClosePress}
                    activeOpacity={0.6}
                >
                    <Icons
                        name="close"
                        color={Colors.purple}
                        size={20}
                    />
                    <Text style={modalHeaderStyle.closeText}>
                        {'بستن'}
                    </Text>
                </TouchableOpacity>
            </View>
            <View style={modalHeaderStyle.rightButton} >
                <Text style={modalHeaderStyle.headerText}>
                    {headerText}
                </Text>
            </View>
        </View>
    );
};

const modalHeaderStyle = EStyleSheet.create({
    '@media (min-width: 600) and (min-height: 900)': {
        header: {
            '@media ios': {
                paddingTop: 25,
                height: hp('9%')
            },
            '@media android': {
                height: hp('7%')
            },
            backgroundColor: '#FFF',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
        },
    },
    header: {
        '@media ios': {
            paddingTop: 21,
            height: hp('12%')
        },
        '@media android': {
            height: hp('9%')
        },
        backgroundColor: '#FFF',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },

    leftButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    rightButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignSelf: 'center',
        marginRight: 10
    },

    closeContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    closeText: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: '#675beb',
        fontSize: normalize(12),
        textAlign: 'center',
        marginLeft: 3
    },
    headerText: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: '#675beb',
        fontSize: normalize(12),
        textAlign: 'center',
        marginRight: 10
    },
    closeIcon: {

    },
});

export default HeaderModal;
