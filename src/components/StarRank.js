import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { moderateScale } from './../responsive/index';

class StarRank extends Component {

    constructor(props) {
        super(props);
        this.state = {
            maxValues: this.props.max || 5,
            value: this.props.value || 0,
        };
        this.direction = this.props.direction || 'ltr';
    }

    _renderStars(starColors) {
        const stars = [];
        const star_half = this.state.value - Math.floor(this.state.value);
        let isMakeHalf = false;
        for (let i = 1; i <= this.state.maxValues; i++) {
            let name = 'star';
            if (this.direction === 'rtl') {
                if (i <= this.state.maxValues - this.state.value) {
                    if (star_half > 0 && isMakeHalf === false) {
                        if (star_half > 0/* && star_half <= 0.5*/) {
                            name = 'star-half-full';
                        } else {
                            name = 'star';
                        }
                        isMakeHalf = true;
                    } else {
                        name = 'star-o';
                    }
                } else {
                    name = 'star';
                }
            }
            else {
                if (this.state.value < i) {
                    if (star_half > 0 && isMakeHalf === false) {
                        if (star_half > 0/* && star_half <= 0.5*/) {
                            name = 'star-half-full';
                        } else {
                            name = 'star';
                        }
                        isMakeHalf = true;
                    } else {
                        name = 'star-o';
                    }
                } else {
                    name = 'star';
                }
            }

            stars.push(
                <FontAwesome
                    name={name}
                    color={starColors || '#FFEB3B'}
                    size={moderateScale(20)}
                    key={i} 
                    style={[this.props.itemStyle]}
                />
            );
        }

        return stars;
    }

    render() {
        return (
            <View
                style={
                    [
                        {
                            flexDirection: 'row',
                            justifyContent: 'center',
                        },
                        this.props.style
                    ]
                }
            >
                {this._renderStars(this.props.starColors)}
            </View>
        );
    }
}

StarRank.defaultProps = {
    direction: 'ltr',
    max: 5,
    value: 1,
    starColors: '#FFEB3B'
};

StarRank.propTypes = {
    style: PropTypes.func,
    itemStyle: PropTypes.func,
    direction: PropTypes.string,
    max: PropTypes.number,
    value: PropTypes.number,
    starColors: PropTypes.any
};

export { StarRank };
