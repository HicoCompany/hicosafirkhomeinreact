import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,ScrollView } from 'react-native';
import { Text,Card, CardItem,Container, Content, Icon,Footer, FooterTab} from 'native-base';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { normalize } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';
import Repository from '../../../repository/Repository';

class DetailPaymentFixedBoothScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'جزئیات اطلاعات همیار'}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };
    constructor(props) {
      super(props);
      this.state = {
          isLoadingPage: true,
          list: [],
          CreateDateStr:'',
          CreateTimeStr:'',
          DeliveryDateStr:'',
          DeliveryTimeStr:'',
          DeliveryWasteID:0,
          StatusStr:'',
          refreshing: false,
          loadingData: false,
          laps:[],
          apiError: false,
          apiErrorDesc: ''
      };
    }
    componentDidMount = async () => {
      var DeliveryWasteID = this.props.navigation.getParam('DeliveryWasteID',0);
     await this.setState({DeliveryWasteID:DeliveryWasteID});
      this.getApi();
  }

  
getApi = async () => {
  await this.PhoneGetHelperDetailsApi();
}
async PhoneGetHelperDetailsApi() {
  const headers = null;
  
  const params = {
    UserName: this.props.userData.UserName,
    Password: this.props.userData.Password,
    DeliveryWasteID:this.state.DeliveryWasteID
  };
  //alert(JSON.stringify(params));
  try {
      const response = await Repository.PhoneGetHelperDetailsApi(params, headers);
      //console.log(response);
      if (response.Success === 1) {
          const laps = response.Result;
          this.setState({
            laps:laps,
            Adress:response.Result.Adress,
            Credit:response.Result.Credit,
            Debit:response.Result.Debit,
            DeliveryDate:response.Result.DeliveryDate,
            HelperName:response.Result.HelperName,
            MainAreaTitle:response.Result.MainAreaTitle,
            Mobile:response.Result.Mobile,
            NationalCode:response.Result.NationalCode,
            Remain:response.Result.Remain,
            Point:response.Result.Point,
            PaymentStatus:response.Result.PaymentStatus
          });
      }
      else {
          this.setState({
              apiError: true,
              apiErrorDesc: response.Text
          });
      }
      this.setState({
          loadingData: false,
          refreshing: false,
          isLoadingPage: false,
      });
  } catch (error) {
      await this.setState({
          loadingData: false,
          refreshing: false,
          isLoadingPage: false,
          apiError: true,
          apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
  }
}

    render() {
        return (
            <Container>
              <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                 <HeaderBack
                        onBackPress={() => this.props.navigation.goBack()}
                        headerTitle={'مشاهده اطلاعات همیار '}
                    />
          <Content>
          <View style={[styles.Title]}>
            <View  style={{flex:9}}>
                <Text style={[styles.textTitle]}> اطلاعات همیار </Text>
            </View>
            <View style={{flex:1,flexDirection:'row',justifyContent:"flex-end",alignItems:'center',}}>
               <Icon type="Entypo" name="shopping-bag" style={{color:'#50b3ae'}}/>
            </View>
          </View>
          <View style={[styles.TotalCard]}>
          <View style={{flexDirection:'column',alignItems: 'center',justifyContent:'center'}}>
          <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.HelperName}</Text>
                <Text style={[styles.text]}> نام همیار:</Text>
               </View>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:5}}>
                <Text style={[styles.text ,{paddingRight:8}]}> {this.state.NationalCode}</Text>
                <Text style={[styles.text]}>کد ملی:</Text>
               </View>
           </View>
           </View>
           <View style={[styles.TotalCard]}>
           <View style={{flexDirection:'column',alignItems: 'center',justifyContent:'center'}}>
           <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.Mobile}</Text>
                <Text style={[styles.text]}>موبایل:</Text>
               </View>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:5}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.MainAreaTitle}</Text>
                <Text style={[styles.text]}>منطقه:</Text>
               </View>
           </View>
           </View>
           <View style={[styles.TotalCard]}>
           <View style={{flexDirection:'column',alignItems: 'center',justifyContent:'center'}}>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.Adress}</Text>
                <Text style={[styles.text]}>آدرس: </Text>
               </View>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.DeliveryDate}</Text>
                <Text style={[styles.text]}>تاریخ جمع آوری:</Text>
               </View>
           </View>
             </View>
           <View style={[styles.TotalCard]}>
           <View style={{flexDirection:'column',alignItems: 'center',justifyContent:'center'}}>
           <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:5}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.PaymentStatus}</Text>
                <Text style={[styles.text]}>وضعیت پرداخت:</Text>
               </View>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.Point}</Text>
                <Text style={[styles.text]}>جمع امتیاز:</Text>
               </View>
           </View>
           </View>
           <View style={[styles.TotalCard]}>
           <View style={{flexDirection:'column',alignItems: 'center',justifyContent:'center'}}>
           <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.Credit}</Text>
                <Text style={[styles.text]}>بستانکار:</Text>
               </View>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:5}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.Debit}</Text>
                <Text style={[styles.text]}>بدهکار:</Text>
               </View>
           </View>
           </View>
           <View style={[styles.TotalCard]}>
           <View style={{flexDirection:'column',alignItems: 'center',justifyContent:'center'}}>
           <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:5}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.Remain}</Text>
                <Text style={[styles.text]}>مانده حساب همیار:</Text>
               </View>
           </View>
           </View>
        </Content>
        <Footer style={{height:70,width:"100%"}}>
          <FooterTab  style={{backgroundColor:'#50b3ae'}}>
           <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <TouchableOpacity 
                style={[styles.BtnFooter]}
                onPress={() => this.props.navigation.navigate("DeliveryTypeScreen",{
                  Mobile:this.state.Mobile
              })}>
                <Text style={[styles.textFooter,{color:'white',}]}>
                      پرداخت
                </Text>
            </TouchableOpacity>
            </View>
          </FooterTab>
          </Footer>
      </Container>
        );
    }

}

  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     header:{
      backgroundColor:'#50b3ae',
     },
      textHeader:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
      text:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
       },
       textTitle:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
       },
       Title:{
        flex:1,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'flex-end',
        paddingTop:15,
        paddingHorizontal:8
       },
       TotalCard:{
        flex:1,
        flexDirection:'column',
        alignItems: 'center',
        justifyContent:'center',
        paddingHorizontal:10,
        paddingTop:10
      },
      CardSelect:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-around',
        borderRadius:5,
        padding:8,
        paddingVertical:12
      },
      IconGreen:{
        color:'#50b3ae', 
        fontSize: 20
      },
      IconBag:{
        color:'#50b3ae',
        fontSize:25
      },
      textFooter:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
       BtntTime:{
           justifyContent:'center',
           alignItems:'center',
           
       },
       CardTime:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-between',
        borderRadius:5,
        paddingHorizontal:12,
        paddingVertical:10
      },
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(DetailPaymentFixedBoothScreen);
